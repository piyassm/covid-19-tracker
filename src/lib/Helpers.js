import { Circle, Popup } from "react-leaflet";

const casesTypeColors = {
  cases: {
    hex: "#CC1034",
    multiplier: 800,
  },
  recovered: {
    hex: "#7dd71d",
    multiplier: 1200,
  },
  deaths: {
    hex: "#fb4443",
    multiplier: 2000,
  },
};

export const removeCommaNumber = (val) => {
  if (val) {
    return val.toString().replace(/,/g, "");
  }
  return val;
};

export const addCommaNumber = (val) => {
  if (val) {
    let data = removeCommaNumber(val);
    data = data
      .replace(".", "x")
      .replace(/\./g, "")
      .replace("x", ".")
      .split(".");
    data[0] = parseInt(data[0])
      .toString()
      .replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    data = data.join(".");
    return data;
  }
  return val;
};

export const sortData = (data) => {
  const sortedData = [...data];
  sortedData.sort((a, b) => {
    if (a.cases > b.cases) {
      return -1;
    }
    return 1;
  });
  return sortedData;
};

export const showDataOnMap = (data, casesType = "cases") =>
  data.map((country, key) => (
    <Circle
      key={key}
      center={[country.countryInfo.lat, country.countryInfo.long]}
      color={casesTypeColors[casesType].hex}
      fillColor={casesTypeColors[casesType].hex}
      fillOpacity={0.4}
      radius={
        Math.sqrt(country[casesType]) * casesTypeColors[casesType].multiplier
      }
    >
      <Popup>
        <div className="info-container">
          <div
            className="info-flag"
            style={{ backgroundImage: `url(${country.countryInfo.flag})` }}
          ></div>
          <div className="info-name">{country.country}</div>
          <div className="info-confirmed">
            Cases: {addCommaNumber(country.cases)}
          </div>
          <div className="info-recovered">
            Recovered: {addCommaNumber(country.recovered)}
          </div>
          <div className="info-deaths">
            Deaths: {addCommaNumber(country.deaths)}
          </div>
        </div>
      </Popup>
    </Circle>
  ));
