import React from "react";
import "./InfoBox.css";
import { Card, CardContent, Typography } from "@material-ui/core";
import { addCommaNumber } from "./lib/Helpers";

const InfoBox = (props) => {
  const { title, cases, total, active, isRed } = props;
  return (
    <Card
      className={`infoBox ${active && "infoBox--selected"} ${
        isRed && "infoBox--red"
      }`}
      onClick={props.onClick}
    >
      <CardContent>
        <Typography color={"textSecondary"} className="infoBox__title">
          {title}
        </Typography>
        <h2 className={`infoBox__cases ${!isRed && "infoBox__cases--green"}`}>
          {addCommaNumber(cases)}
        </h2>
        <Typography color={"textSecondary"} className="infoBox__total">
          {addCommaNumber(total)} Total
        </Typography>
      </CardContent>
    </Card>
  );
};

export default InfoBox;
