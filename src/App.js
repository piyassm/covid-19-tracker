import "./App.css";
import { useEffect, useState } from "react";
import InfoBox from "./InfoBox";
import Map from "./Map";
import { Card, CardContent } from "@material-ui/core";
import Select from "react-select";
import Table from "./Table";
import LineGraph from "./LineGraph";
import "leaflet/dist/leaflet.css";

const App = () => {
  const [countries, setCountries] = useState(["USA", "THAILAND"]);
  const [country, setCountry] = useState({
    value: "worldwide",
    label: "Worldwide",
  });
  const [countryInfo, setCountryInfo] = useState({});
  const [tableData, setTableData] = useState([]);
  const [mapCountries, setMapCountries] = useState([]);
  const [casesType, setCasesType] = useState("cases");
  const [mapCenter, setMapCenter] = useState({ lat: 34.80746, lng: -40.4796 });
  const [mapZoom, setMapZoom] = useState(3);

  useEffect(() => {
    const getCountriesData = async () => {
      await fetch("https://disease.sh/v3/covid-19/countries")
        .then((response) => response.json())
        .then((data) => {
          const countries = data.map((country) => ({
            name: country.country,
            value: country.countryInfo.iso2,
          }));
          setCountries(countries);
          setMapCountries(data);
          setTableData(data);
        });
    };
    getCountriesData();
  }, []);

  const onCountryChange = async (e) => {
    setCountry(e);
  };

  useEffect(() => {
    const getCountryInfo = async () => {
      const value = country.value;
      const url =
        value === "worldwide"
          ? "https://disease.sh/v3/covid-19/all"
          : `https://disease.sh/v3/covid-19/countries/${value}`;

      await fetch(url)
        .then((response) => response.json())
        .then((data) => {
          setCountryInfo(data);
          if (data?.countryInfo) {
            setMapCenter([data.countryInfo.lat, data.countryInfo.long]);
          }
          setMapZoom(4);
        });
    };
    getCountryInfo();
  }, [country]);
  return (
    <div className="app">
      <div className="app__left">
        <div className="app__header">
          <h1>COVID-19 TRACKER</h1>
          <div className="app__countries">
            <Select
              value={country}
              onChange={onCountryChange}
              options={[
                {
                  value: "worldwide",
                  label: "Worldwide",
                },
                ...countries.map((country, key) => ({
                  value: country.value,
                  label: country.name,
                })),
              ]}
            />
          </div>
        </div>
        <div className="app_stats">
          <InfoBox
            onClick={(e) => setCasesType("cases")}
            title={"Coronavirus Case"}
            active={casesType === "cases"}
            isRed
            cases={countryInfo?.todayCases}
            total={countryInfo?.cases}
          ></InfoBox>
          <InfoBox
            onClick={(e) => setCasesType("recovered")}
            title={"Recovered"}
            active={casesType === "recovered"}
            cases={countryInfo?.todayRecovered}
            total={countryInfo?.recovered}
          ></InfoBox>
          <InfoBox
            onClick={(e) => setCasesType("deaths")}
            title={"Deaths"}
            active={casesType === "deaths"}
            isRed
            cases={countryInfo?.todayDeaths}
            total={countryInfo?.deaths}
          ></InfoBox>
        </div>

        <Map
          countries={mapCountries}
          casesType={casesType}
          center={mapCenter}
          zoom={mapZoom}
        />
      </div>
      <Card className="app__right">
        <CardContent>
          <h3>Live Cases by Country</h3>
          <Table countries={tableData} />
          <h3 className="app__graphTitle">Worldwide new {casesType}</h3>
          <LineGraph className="app__graph" casesType={casesType} />
        </CardContent>
      </Card>
    </div>
  );
};

export default App;
