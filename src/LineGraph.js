import React, { useEffect, useState } from "react";
import "./LineGraph.css";
import { Line } from "react-chartjs-2";
import { addCommaNumber } from "./lib/Helpers";

const options = {
  legend: {
    display: false,
  },
  elements: {
    point: {
      radius: 0,
    },
  },
  maintainAspectRatio: false,
  tooltips: {
    mode: "index",
    intersect: false,
    callbacks: {
      label: function (tooltipItem, data) {
        return addCommaNumber(tooltipItem.value);
      },
    },
  },
  scales: {
    xAxes: [
      {
        type: "time",
        time: {
          parser: "MM/DD/YY",
          tooltipFormat: "ll",
        },
      },
    ],
    yAxes: [
      {
        gridLines: {
          display: false,
        },
        ticks: {
          callback: function (value, index, values) {
            return addCommaNumber(value);
          },
        },
      },
    ],
  },
};

const LineGraph = (props) => {
  const { casesType = "cases" } = props;
  const [data, setData] = useState([]);

  useEffect(() => {
    const fecthData = async () => {
      await fetch("https://disease.sh/v3/covid-19/historical/all?lastdays=60")
        .then((response) => response.json())
        .then((data) => {
          const chartData = buildChartData(data);
          setData(chartData);
        });
    };
    fecthData();
  }, [casesType]);

  const buildChartData = (data) => {
    const chartData = [];
    let lastDataPoint;
    for (let date in data?.cases) {
      if (data[casesType][date] > lastDataPoint) {
        const newDataObject = {
          x: date,
          y: data[casesType][date] - lastDataPoint,
        };
        chartData.push(newDataObject);
      } else {
        const newDataObject = {
          x: date,
          y: 0,
        };
        chartData.push(newDataObject);
      }
      lastDataPoint = data[casesType][date];
    }

    return chartData;
  };

  return (
    <div className="lineGraph">
      {!!data?.length && (
        <Line
          data={{
            datasets: [
              {
                backgroundColor: "rgba(204, 16, 52, 0.5)",
                borderColor: "#CC1034",
                data: data,
              },
            ],
          }}
          options={options}
        />
      )}
    </div>
  );
};

export default LineGraph;
