import React from "react";
import "./Table.css";
import { addCommaNumber, sortData } from "./lib/Helpers";

const Table = (props) => {
  const { countries } = props;
  const sortCountries = sortData(countries);
  return (
    <div className="table">
      <table>
        <tbody>
          {sortCountries.map((country, key) => (
            <tr key={key}>
              <td>{country.country}</td>
              <td>{addCommaNumber(country.cases)}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
