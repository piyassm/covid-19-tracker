FROM node:14-alpine

WORKDIR /app
COPY package*.json ./
EXPOSE 3000

RUN npm install yarn && yarn install
RUN npm install -g firebase-tools
COPY . .
CMD ["yarn", "start"]